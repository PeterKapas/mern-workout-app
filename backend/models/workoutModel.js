const mongoose = require('mongoose')

const workoutSchema = mongoose.Schema(
	{
		title: {
			type: String,
			required: [true, 'Please add a workout name'],
		},
		reps: {
			type: Number,
			required: [true, 'Please add a rep count'],
		},
		load: {
			type: Number,
			required: [true, 'Please add a load'],
		},
	},
	{ timestamps: true }
)

module.exports = mongoose.model('Workout', workoutSchema)
