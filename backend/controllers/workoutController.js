const Workout = require('../models/workoutModel')
const mongoose = require('mongoose')

const getWorkouts = async (req, res) => {
	try {
		const workouts = await Workout.find({}).sort({ updatedAt: -1 })
		res.status(200).json(workouts)
	} catch (error) {
		res.status(400).json({ error: error.message })
	}
}

const getWorkout = async (req, res) => {
	const { id } = req.params
	try {
		const singleWorkout = await Workout.findById(id)
		if (!singleWorkout) {
			return res.status(404).json({ error: 'No such workout' })
		}
		res.status(200).json(singleWorkout)
	} catch (error) {
		res.status(400).json({ error: 'No such workout' })
	}
}

const createWorkout = async (req, res) => {
	const { title, load, reps } = req.body

	let emptyFields = []
	if (!title) {
		emptyFields.push('title')
	}
	if (!load) {
		emptyFields.push('load')
	}
	if (!reps) {
		emptyFields.push('reps')
	}
	if (emptyFields.length > 0) {
		return res
			.status(400)
			.json({ error: 'Please fill in all the fields', emptyFields })
	}

	try {
		const workout = await Workout.create({ title, load, reps })
		res.status(200).json(workout)
	} catch (error) {
		res.status(400).json({ error: error.message })
	}
}

const deleteWorkout = async (req, res) => {
	const { id } = req.params
	try {
		const deletedWorkout = await Workout.findByIdAndDelete(id)
		res.status(200).json(deletedWorkout)
	} catch (error) {
		res.status(400).json({ error: 'No such workout to delete' })
	}
}

const updateWorkout = async (req, res) => {
	const { id } = req.params
	try {
		const updatedWorkout = await Workout.findByIdAndUpdate(
			id,
			{
				...req.body,
			},
			{ new: true }
		)
		res.status(200).json(updatedWorkout)
	} catch (error) {}
}

/* const deleteAllWorkouts = async (req, res) => {
	try {
		await Note.deleteMany({ id })
		res.status(200)
	} catch (error) {
		res.status(400).json({ error: 'Could not delete all workouts' })
	}
} */

module.exports = {
	getWorkouts,
	getWorkout,
	createWorkout,
	deleteWorkout,
	updateWorkout,
}
