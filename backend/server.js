const dotenv = require('dotenv').config()
const express = require('express')
const connectDB = require('./config/db')
const workoutRoutes = require('./routes/workoutRoutes')

connectDB()

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use((req, res, next) => {
	console.log(req.path, req.method)
	next()
})

app.use('/api/workouts', workoutRoutes)

const port = process.env.PORT || 5000

app.listen(port, () => console.log(`Server started on port ${port}...`))
