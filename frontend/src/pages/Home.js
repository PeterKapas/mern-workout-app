import { useEffect, useState } from 'react'
import axios from 'axios'
import WorkoutCard from '../components/WorkoutCard'
import WorkoutForm from '../components/WorkoutForm'
import { useWorkoutsContext } from '../hooks/useWorkoutsContext'
import Modal from '../components/Modal'

const Home = () => {
	const { workouts, dispatch } = useWorkoutsContext()
	const [toogleModal, setToogleModal] = useState(false)

	useEffect(() => {
		const fetchWorkouts = async () => {
			const response = await axios.get('/api/workouts/')
			dispatch({ type: 'SET_WORKOUTS', payload: response.data })
		}
		fetchWorkouts()
	}, [dispatch])

	const toogle = () => {
		setToogleModal((prev) => !prev)
	}

	if (toogleModal) {
		return <Modal toogleModal={toogleModal} />
	}

	return (
		<div className='home'>
			<div className='workouts'>
				{workouts &&
					workouts.map((workout) => (
						<WorkoutCard
							key={workout._id}
							workout={workout}
							setToogleModal={setToogleModal}
							toogle={toogle}
						/>
					))}
				<WorkoutForm />
			</div>
		</div>
	)
}
export default Home
