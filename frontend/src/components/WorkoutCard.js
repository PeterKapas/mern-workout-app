import axios from 'axios'
import { useState, useRef } from 'react'
import { useWorkoutsContext } from '../hooks/useWorkoutsContext'
import { FaTrashAlt, FaRegEdit, FaRegCheckCircle } from 'react-icons/fa'
import formatDistanceToNow from 'date-fns/formatDistanceToNow'

const WorkoutCard = ({ workout, setToogleModal, toogle }) => {
	const { dispatch } = useWorkoutsContext()

	const handleDelete = async () => {
		const response = await axios.delete('/api/workouts/' + workout._id)
		dispatch({ type: 'DELETE_WORKOUT', payload: response.data })
	}

	const handleUpdate = async () => {
		const response = await axios.patch('/api/workouts/' + workout._id)
		dispatch({ type: 'UPDATE_WORKOUT', payload: response.data })
		console.log(response.data)
	}

	return (
		<>
			<div className='workout-details'>
				<h4>{workout.title}</h4>
				<p>
					<strong>Load (kg): </strong>
					{workout.load}
				</p>
				<p>
					<strong>Reps: </strong>
					{workout.reps}
				</p>
				<p>
					{formatDistanceToNow(new Date(workout.updatedAt), {
						addSuffix: true,
					})}
				</p>
				<button onClick={handleDelete}>
					<FaTrashAlt />
				</button>
				<button onClick={toogle}>
					<FaRegEdit />
				</button>
			</div>
			<div>
				<p>Update</p>
			</div>
		</>
	)
}

export default WorkoutCard
