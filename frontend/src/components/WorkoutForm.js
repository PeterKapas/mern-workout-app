import axios from 'axios'
import { useState } from 'react'
import { useWorkoutsContext } from '../hooks/useWorkoutsContext'

const WorkoutForm = () => {
	const { dispatch } = useWorkoutsContext()
	const {
		state: { workouts, title, load, reps, error, emptyFields },
	} = useWorkoutsContext()

	/* 	const [title, setTitle] = useState('')
	const [load, setLoad] = useState('')
	const [reps, setReps] = useState('')
	const [error, setError] = useState(null)
	const [emptyFields, setEmptyFields] = useState([]) */

	const handleSubmit = async (e) => {
		e.preventDefault()
		try {
			const workout = { title, load, reps }
			const response = await axios.post('/api/workouts', workout)
			dispatch({ error: initialState.error })
			dispatch({ title: initialState.title })
			dispatch({ load: initialState.load })
			dispatch({ reps: initialState.reps })
			dispatch({ error: initialState.error })
			dispatch({ error: initialState.error })
			dispatch({ type: 'CREATE_WORKOUT', payload: response.data })
			dispatch({ emptyFields: initialState.emptyFields })
		} catch (error) {
			if (error.response) {
				console.log(error.response)
				console.log(error.response.data)
				dispatch({ error: error.response.data.error })
				dispatch({ emptyFields: error.response.data.emptyFields })
			}
		}
	}

	const handleInput = (e) => {
		dispatch({ [e.target.name]: e.target.value })
	}
	return (
		<form className='create-form' onSubmit={handleSubmit}>
			<h3>Add a New Workout</h3>

			<label>Excercize Title:</label>
			<input
				name='title'
				type='text'
				onChange={handleInput}
				value={title}
				className={emptyFields.includes('title') ? 'error' : ''}
			/>

			<label>Load (in kg):</label>
			<input
				name='load'
				type='number'
				onChange={handleInput}
				value={load}
				className={emptyFields.includes('load') ? 'error' : ''}
			/>

			<label>Number of Reps:</label>
			<input
				name='reps'
				type='number'
				onChange={handleInput}
				value={reps}
				className={emptyFields.includes('reps') ? 'error' : ''}
			/>

			<button>Add Workout</button>
			{error && <div className='error'>{error}</div>}
		</form>
	)
}

export default WorkoutForm
