import { Link } from 'react-router-dom'

const NavBar = () => {
	return (
		<nav>
			<div className='container'>
				<Link to='/'>
					<h1>Workout Tracker</h1>
				</Link>
			</div>
		</nav>
	)
}
export default NavBar
