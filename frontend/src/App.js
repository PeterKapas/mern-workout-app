import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import NavBar from './components/NavBar'
import Home from './pages/Home'

function App() {
	return (
		<div className='App'>
			<Router>
				<NavBar />
				<div className='pages'>
					<Routes>
						<Route path='/' element={<Home />} />
					</Routes>
				</div>
			</Router>
		</div>
	)
}

export default App
