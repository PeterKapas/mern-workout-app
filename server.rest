GET http://localhost:5000/api/workouts/

###
GET http://localhost:5000/api/workouts/6335a7bbdf161347b9bb5d78

###
POST  http://localhost:5000/api/workouts/
Content-Type: application/json; charset=utf-8

{
  "title":"decline bench",
  "load":"40",
  "reps":"10"
}

###
DELETE http://localhost:5000/api/workouts/6335a7bbdf161347b9bb5d78
Content-Type: application/json; charset=utf-8

###
PATCH  http://localhost:5000/api/workouts/6335a7bbdf161347b9bb5d78
Content-Type: application/json; charset=utf-8

{
  "load":"30",
  "reps":"20"
}

###
